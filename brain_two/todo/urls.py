from django.urls import path
from todo.views import list_TodoList, show_Todo_list_detail

urlpatterns = [
    path("todo/", list_TodoList, name = "todo_list_list"),
    path("todo/<int:id>/", show_Todo_list_detail, name = "todo_list_detail")
]
