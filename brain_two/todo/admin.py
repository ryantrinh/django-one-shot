from django.contrib import admin
from todo.models import TodoList, TodoItem

# Register your models here.
@admin.register(TodoList)
class TodoAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id"
    )

@admin.register(TodoItem)
class TodoAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date"
    )
