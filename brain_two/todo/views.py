from django.shortcuts import render, get_object_or_404
from todo.models import TodoList


# Create your views here.

def list_TodoList(request):

    lists = TodoList.objects.all()

    context = {
        "all_list": lists
    }
    return render(request, "todo/list.html", context)

def show_Todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_list_detail
    }
    return render(request, "todo/todo_list_detail.html", context)
